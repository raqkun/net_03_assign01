﻿using NET03_ASSIGN01.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace NET03_ASSIGN01
{
    class Program
    {
        public static GiaoVien minSalary(List<GiaoVien> giaoViens)
        {
            GiaoVien agv = new GiaoVien();
            giaoViens.Sort(delegate (GiaoVien x, GiaoVien y) {
                return x.TinhLuong().CompareTo(y.TinhLuong());
            });
            agv = giaoViens.First();
            return agv;
        }
        static void Main(string[] args)
        {
            List<GiaoVien> giaoViens = new List<GiaoVien>();
            int n =0;
            Console.Write("Nhap so luong Giao Vien: ");
            //Check n is int and > 0
            nChecking:
            while (!(int.TryParse(Console.ReadLine(), out n)))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("<!> So luong phai la so tu nhien <!>");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Nhap lai so luong: ");
            }
            if (n <= 0) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("<!> So luong phai la so tu nhien lon hon 0 <!>");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Nhap lai so luong: ");
                goto nChecking;
            }
            //Start input list
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("-------[{0}]-------", i + 1);
                GiaoVien newgv = new GiaoVien();
                Console.Write("Ho Ten: ");
                string hoten = Console.ReadLine();
                // Check hoten != null
                while (hoten == "" || hoten == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> Ho ten khong duoc trong <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai Ho Ten: ");
                    hoten = Console.ReadLine();
                }
                Console.Write("Nam Sinh: ");
                int namsinh;
                // Check namsinh is int and >0
                yearChecking:
                while (!(int.TryParse(Console.ReadLine(), out namsinh)))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> Nam Sinh phai la so tu nhien <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai Nam Sinh: ");
                }
                if (namsinh <= 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> Nam sinh phai la so tu nhien lon hon 0 <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai Nam Sinh: ");
                    goto yearChecking;
                }
                Console.Write("Luong co ban: ");
                double luongcoban;
                salaChecking:
                // Check luongcoban is double and >= 0
                while (!(double.TryParse(Console.ReadLine(), out luongcoban)))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> Luong co ban phai la double <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Luong co ban: ");
                }
                if (luongcoban < 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> Luong co ban phai lon hon hoac bang 0 <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai Luong co ban: ");
                    goto salaChecking;
                }
                Console.Write("He so luong: ");
                double hesoluong;
                // Check hesoluong is double and >= 0
                hesoChecking:
                while (!(double.TryParse(Console.ReadLine(), out hesoluong)))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> He so luong phai la double <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai He so luong: ");
                }
                if (hesoluong < 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("<!> He so luong phai lon hon hoac bang 0 <!>");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Nhap lai He so luong: ");
                    goto hesoChecking;
                }
                newgv.NhapThongTin(hoten, namsinh, luongcoban);
                newgv.NhapThongTin(hesoluong);
                giaoViens.Add(newgv);
            }
            GiaoVien result = minSalary(giaoViens);
            Console.WriteLine("------[ Giao Vien Co Luong Thap Nhat ]------");
            result.XuatThongtin();
            Console.Read();


        }
    }
}