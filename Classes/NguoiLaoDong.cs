﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET03_ASSIGN01.Classes
{
    public class NguoiLaoDong
    {
        public string HoTen = "";
        public int NamSinh;
        public double LuongCoBan;

        public NguoiLaoDong(string HoTen, int NamSinh, double LuongCoBan)
        {
            this.HoTen = HoTen;
            this.NamSinh = NamSinh;
            this.LuongCoBan = LuongCoBan;
        }
        public NguoiLaoDong(){}
        ~NguoiLaoDong() { }

        public void NhapThongTin(string HoTen, int NamSinh, double LuongCoBan)
        {
            this.HoTen = HoTen;
            this.NamSinh = NamSinh;
            this.LuongCoBan = LuongCoBan;
        }

        public virtual double TinhLuong()
        {
            return this.LuongCoBan;
        }

        public virtual void XuatThongtin()
        {
            Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban: {2}.",this.HoTen,this.NamSinh,this.LuongCoBan);
        }


    }
}
