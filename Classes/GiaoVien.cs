﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET03_ASSIGN01.Classes
{
    public class GiaoVien:NguoiLaoDong
    {
        private double HeSoLuong;
        public GiaoVien(string HoTen, int NamSinh, double LuongCoBan, double HeSoLuong) 
        {
            this.HoTen = HoTen; ;
            this.NamSinh = NamSinh;
            this.LuongCoBan = LuongCoBan;
            this.HeSoLuong = HeSoLuong;
        }

        public void NhapThongTin(double HeSoLuong)
        {
            this.HeSoLuong = HeSoLuong;
        }

        public override double TinhLuong()
        {
            return this.HeSoLuong*this.LuongCoBan*1.25;
        }
        public override void XuatThongtin()
        {
            Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban: {2}, he so luong: {3}, luong: {4}", this.HoTen, this.NamSinh, this.LuongCoBan, this.HeSoLuong, this.TinhLuong());
        }
        public void Xuli()
        {
            this.HeSoLuong = this.HeSoLuong + 0.6;
        }

        public GiaoVien() { }
        ~GiaoVien() { }
    }
}
